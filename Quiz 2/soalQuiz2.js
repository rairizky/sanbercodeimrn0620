// Soal 1
console.log("\n===== Soal 1 ====\n");

class Score {
  // Code disini
  constructor(subject, points, email) {
        this.subject = subject;
        this.points = points;
        this.email = email;
  }

  objScoreQuiz() {
        let objData = {};
        objData.email = this.email;
        objData.subject = this.subject;
        objData.points = this.points;
        return objData
  }

  average() {
        if (this.points.length > 1) {
            let point = 0;
            
            this.points.forEach(points => {
                point += points ;
            });
            
            let result = point / this.points.length;
            return `Rata-rata point ${this.subject} untuk ${this.email} adalah ${result.toFixed(1)}`;
        } else {
            return "Point tidak bisa di rata-rata";
        }
  }
}

let points = [60, 70, 90];
let score = new Score('React Native', points, 'rairizky37@gmail.com');
console.log(score.average());
// end Soal 1

// Soal 2
console.log("\n===== Soal 2 =====\n");

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
    // code kamu di sini
    const result = [];

    for(let i=0; i<data.length; i++) {
        if (data[i][0] != "email") {
            let email = data[i][0];
            let point = 0;

            if(subject === "quiz-1") {
                point = data[i][1];
            }
            else if (subject === "quiz-2") {
                point = data[i][2];
            }
            else if (subject === "quiz-3") {
                point = data[i][3];
            }

            let inputToClass = new Score(subject, point, email);
            result.push(inputToClass.objScoreQuiz()); // get object data without name object
        } 
    }
    console.log(result);
}

// TEST CASE
viewScores(data, "quiz-1");
viewScores(data, "quiz-2");
viewScores(data, "quiz-3");
// end Soal 2

// Soal 3
console.log("\n===== Soal 3 =====\n");

function recapScores(data) {
  // code kamu di sini
  for(let i=0; i<data.length; i++) {
        if (data[i][0] != "email") {
            let email = data[i][0];
        let sumPoint = data[i][1]+data[i][2]+data[i][3];
        let rata2 = sumPoint/3;
        let predikat = "";
        console.log(`${i}. Email\t: ${email}`);
        if (rata2.toFixed(1) % 1) {
            console.log(`Rata-rata\t: ${rata2.toFixed(1)}`);
        } else {
            console.log(`Rata-rata\t: ${rata2.toFixed(0)}`);
        }

        if (rata2 > 90) {
            predikat = "honour";
        } else if (rata2 > 80) {
            predikat = "graduate";
        } else if (rata2 > 70) {
            predikat = "participant"
        }
        console.log(`Predikat\t: ${predikat}\n`);
        }
  }
}

recapScores(data);
// end Soal 3

