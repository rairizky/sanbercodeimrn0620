// Soal 1 
console.log("\n===== Soal 1 =====\n");

const golden = () => {
    console.log("this is golden!");
}

golden();
// end Soal 1

// Soal 2
console.log("\n===== Soal 2 =====\n");

const newFunction = ( firstName, lastName ) => {
    return {
        firstName,
        lastName,
        fullName: () => {
            console.log(firstName, lastName);
        }
    };
};

newFunction("William", "Imoh").fullName();
// end Soal 2

// Soal 3
console.log("\n===== Soal 3 =====\n");

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
};

const { firstName, lastName, destination, occupation } = newObject;
console.log(firstName, lastName, destination, occupation);
// end Soal 3

// Soal 4
console.log("\n===== Soal 4 =====\n");

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];

console.log(combined);
// end Soal 4

// Soal 5
console.log("\n===== Soal 5 =====\n");

const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do 
eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;

console.log(before);
//end Soal 5