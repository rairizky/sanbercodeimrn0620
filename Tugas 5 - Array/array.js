// Soal 1
console.log("\n===== Soal 1 =====\n");

function range(startNum, finishNum) {
    var arrayData = [];
    if (!startNum || !finishNum) {
        return -1;
    } else {
        if (startNum < finishNum) {
            for(var i=startNum; i<=finishNum; i++) {
                arrayData.push(i);
            }

            return arrayData;
        } else if (startNum > finishNum) {
            for(var i=startNum; i>=finishNum; i--) {
                arrayData.push(i);
            }

            return arrayData;
        }
    }
}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());
// end Soal 1

// Soal 2
console.log("\n===== Soal 2 =====\n");

function rangeWithStep(startNum, finishNum, step) {
    var arrayData = [];
    if (!startNum || !finishNum || !step) {
        return -1;
    } else {
        if (startNum < finishNum) {
            for(var i=startNum; i<=finishNum; i+=step) {
                arrayData.push(i);
            }
            return arrayData;
        } else if (startNum > finishNum) {
            for(var i=startNum; i>=finishNum; i-=step) {
                arrayData.push(i);
            }

            return arrayData;
        }
    }
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));
// end Soal 2

// Soal 3
console.log("\n===== Soal 3 =====");

function sum(params1, params2, step=1) {
    var jumlah = 0;
    if (!params1 && !params2) {
        return 0;
    } else if (!params1 || !params2)  {
        return 1;
    } else {
        if (params1 < params2) {
            for(var i=params1; i<=params2; i+=step) {
                jumlah = jumlah+i;
            }
            return jumlah;
        } else if (params1 > params2) {
            for(var i=params2; i<=params1; i+=step) {
                jumlah = jumlah+i;
            }
            return jumlah;
        }
    }
}

console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());
// end Soal3

// Soal 4
console.log("\n===== Soal 4 =====\n");

function dataHandling() {
    var input = [
                    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
                ];

    for(var i=0; i<input.length; i++) {
        console.log(`Nomor ID: ${input[i][0]}`);
        console.log(`Nama Lengkap: ${input[i][1]}`);
        console.log(`TTL: ${input[i][2]}, ${input[i][3]}`);
        console.log(`Hobi: ${input[i][4]}\n`);
    }
}
dataHandling();
// end Soal 4

// Soal 5
console.log("===== Soal 5 =====\n");

function balikKata(params) {
    var string = params;
    var result = "";
    for(var i=params.length - 1; i>=0; i--) {
        result = result + string[i];
    }
    return result;
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));
// end Soal 5

// Soal 6
console.log("\n===== Soal 6 =====\n");


function dataHandling2(paramsData) {
    paramsData.splice(1,2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    paramsData.splice(4,1, "Pria", "SMA Internasional Metro");
    console.log(paramsData);
    
    var dob = paramsData[3].split('/');
    var dob1 = paramsData[3].split('/');
    
    switch (parseInt(dob[1])) {
        case 1:
            console.log("Januari");
            break;
        case 2:
            console.log("Februari");
            break;
        case 3:
            console.log("Maret");
            break;
        case 4:
            console.log("April");
            break;
        case 5:
            console.log("Mei");
            break;                    
        case 6:
            console.log("Juni");
            break; 
        case 7:
            console.log("Juli");
            break;
        case 8:
            console.log("Agustus");
            break;
        case 9:
            console.log("September");
            break;
        case 10:
            console.log("Oktober");
            break;
        case 11:
            console.log("November");
            break;                    
        case 12:
            console.log("Desember");
            break;    
        default:
            console.log("Bulan hanya dari 1-12");
            break;
    }

    var descDate = dob.sort(function(a, b) { return b - a });
    console.log(descDate);

    var joinDate = dob1.join("-");
    console.log(joinDate);
    
    var getNamed = paramsData[1];
    var sliceName = getNamed.slice(0, 15);
    console.log(sliceName);
    
}

var data = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(data);
// end Soal 6