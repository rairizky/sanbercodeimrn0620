var readBooks = require('./callback');

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var increment = 0;
var waktu = 10000;
readBooks(waktu, books[increment], function out(call) {
    if (call != waktu) {
        increment++;
        waktu = call;
        if (call > 0 && increment < books.length) {
            readBooks(waktu, books[increment], out);
        } else {
            console.log(call);
        }
    }
});
