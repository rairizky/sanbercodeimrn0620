var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var increment = 0;
var waktu = 10000;
readBooksPromise(10000, books[increment])
    .then(function out(call) {
        if (call != waktu) {
            increment++;
            waktu = call;
            if (call > 0 && increment < books.length) {
                readBooksPromise(waktu, books[increment])
                    .then(out)
                    .catch(function(error) {
                        console.log(error.message);
                    })
            } else {
                console.log(call);
            }
        }
    })
    .catch(function(error) {
        console.log(error.message);
    })
