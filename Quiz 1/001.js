//
function balikString(paramsKata) {
  var result = "";
  for (var i=paramsKata.length-1; i>=0; i--) {
      result = result + paramsKata[i];
  }
  return result;
}

function palindrome(paramsKata) {
  var result = "";
  for (var i=paramsKata.length-1; i>=0; i--) {
      result = result + paramsKata[i];
  }

  if (paramsKata == result) {
      return true;
  } else {
      return false
  }
}

function bandingkan(num1, num2) {
  var parseNum1 = parseInt(num1);
  var parseNum2 = parseInt(num2);

  if (!num1 && !num2) {
      return -1;
  } else if (!num1 || !num2) {
      return 1;
  } else if (parseNum1 < 0 || parseNum2 < 0) {
      return -1;
  } else if(parseNum1 == parseNum2) {
      return -1;
  } else {
      if (parseNum1 > parseNum2) {
          return parseNum1;
      } else if (parseNum1 < parseNum2) {
          return parseNum2;
      }
  }
}

// TEST CASES BalikString
console.log("\n===== A =====\n")

console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

// TEST CASES Palindrome
console.log("\n===== B =====\n");


console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false

// // TEST CASES Bandingkan Angka
console.log("\n===== C =====\n");

console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18
