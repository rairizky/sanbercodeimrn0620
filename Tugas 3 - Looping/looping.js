// Soal 1
console.log("\n===== While =====");
console.log("\nLooping Pertama");
var data1 = 2;
while(data1 < 22) {
    console.log(data1 + " - I love coding");
    data1+=2;
}

console.log("\nLooping Kedua");
var data2 = 20;
while(data2 > 1) {
    console.log(data2 + " - I will become a mobile developer");
    data2 -= 2;
}
// end Soal 1

// Soal 2
console.log("\n===== For =====\n");
for(var data3 = 1; data3 <= 20; data3++) {
    if(data3 % 3 == 0 && data3 % 2 == 1) {
        console.log(data3 + " - I Love Coding");
    } else if(data3 % 2 == 0) {
        console.log(data3 + " - Berkualitas");
    } else if(data3 % 2 == 1) {
        console.log(data3 + " - Santai");
    }
}
// end Soal 2

// Soal Persegi Panjang
console.log("\n===== Persegi Panjang =====\n");
var persegi = ""
for(var i = 0; i < 4; i++) {
    for(var j=0; j<8; j++) {
        persegi = persegi+"#";
    }
    persegi = persegi + "\n";
}
console.log(persegi);
// end Soal Persegi Panjang

// Soal Tangga
console.log("\n===== Tangga =====\n");
for (var i = 1; i <= 7; i++) {
	var pagarTangga = '';
	for (var j = 1; j <= i; j++) {
        pagarTangga += "#";
	}
	console.log(pagarTangga);
}
// end Soal Persegi Panjang

// Soal Papan Catur
console.log("\n===== Papan Catur =====\n");
var a = "#"
var b = " ";
var hasil = " ";
for(var i=1 ; i<=8; i++ )
{
    for(var j=1; j<=8; j++) {
        if(i%2 == 0) {
            if(j%2 == 1) {
                hasil = hasil + a;
            } else {
                hasil = hasil + b;
            }
        } else {
            if(j%2 == 1) {
                hasil = hasil + b;
            } else {
                hasil = hasil + a;
            }
        }
    }
    hasil = hasil + " \n ";
}
console.log(hasil);
// end Soal Persegi Panjang