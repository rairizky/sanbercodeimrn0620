// Soal 1 
console.log("\n===== Soal 1 =====\n");

function arrayToObject(arr) {
    for(var i=0; i<arr.length; i++) {
        var date = new Date();
        var thisYear = date.getFullYear();
        var yearOfBirth = arr[i][3];
        var resultAge;

        if (!yearOfBirth || yearOfBirth > thisYear) {
            resultAge = "Invalid Birth Year";
        } else {
            var getAge = thisYear - yearOfBirth;
            resultAge = getAge;
        }
        var arrayToObject = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: resultAge
        }
        var obj = arrayToObject;
        var num = i+1;
        console.log(`${num}. ${obj.firstName} ${obj.lastName}:`, obj);
    }
}

var people = [["Bruce", "Branner", "male", 1975], ["Natasha", "Romanoff", "female"]];
arrayToObject(people);

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ];
arrayToObject(people2);

arrayToObject([]);
// End Soal 1

// Soal 2
console.log("\n===== Soal 2 =====\n");

function shoppingTime(memberId, money) {
    if (!memberId) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup";
    } else {
        var nota = {
            memberId: memberId,
            money: money,
            listPurchased: [],
            changeMoney: 0
        }
        var isPurchase = true;
        while(money > 0 && isPurchase) {
            if (money >= 1500000) {
                nota.listPurchased.push('Sepatu Stacattu');
                money = money - 1500000;
                isPurchase = true;
            }
            if (money >= 500000) {
                nota.listPurchased.push('Baju Zoro');
                money = money - 500000;
                isPurchase = true;
            }
            if (money >= 250000) {
                nota.listPurchased.push('Baju H&N');
                money = money - 250000;
                isPurchase = true
            }
            if (money >= 175000) {
                nota.listPurchased.push('Sweater Uniklooh');
                money = money - 175000;
                isPurchase = true;
            }
            if (money >= 50000) {
                nota.listPurchased.push('Casing Handphone');
                money = money - 50000;
                isPurchase = true
            }
            isPurchase = false;
        }
        nota.changeMoney = money;
        return nota;
    }
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
// end Soal 2

// Soal 3
console.log("\n===== Soal 3 =====\n");

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var result = [];
    if (!arrPenumpang.length) {
        return result;
    } {
        for(var i=0; i<arrPenumpang.length; i++) {
            var passenger = arrPenumpang[i];
            var objPassenger = {};
            
            objPassenger.penumpang = passenger[0];
            objPassenger.naikDari = passenger[1];
            objPassenger.tujuan = passenger[2];
            // console.log(objPassenger);
            function checkCost(to, from) {
                var cost = 2000;
                // console.log(rute.indexOf(to));
                var costPassenger = cost*(rute.indexOf(to)-rute.indexOf(from));
                return costPassenger;
                
            }
            objPassenger.bayar = checkCost(objPassenger.tujuan, objPassenger.naikDari);
            // console.log(objPassenger);

            result.push(objPassenger);
        }
    }
    return result;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([])); //[]
// end Soal 3