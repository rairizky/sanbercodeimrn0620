// Soal 1 //
console.log("\n===== Soal 1 =====\n");
function teriak() {
    return "Halo Sanbers!";
}

console.log(teriak());
// end Soal 1 //

// Soal 2
console.log("\n===== Soal 2 =====\n");
function kalikan(angka1, angka2) {
    var result = num1*num2;
    return result;
}

var num1 = 12;
var num2 = 4;

console.log(kalikan(12, 4));
// end Soal 2

// Soal 3
console.log("\n===== Soal 3 =====\n");
function introduce(name, age, address, hobby) {
    var pattern = `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!`;

    return pattern;
}

var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";

var data = introduce(name, age, address, hobby);
console.log(data + "\n");
// end Soal 3